/* moogskt.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Code for the library used to communicate with the platform server
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>
#include <stdarg.h>
#include <stdint.h>
#include <ctype.h>

#ifndef MOOG_W
#include <netdb.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#else
#include <winsock.h>
typedef int socklen_t;
#endif

#include <pthread.h>
#include <sys/types.h>
#include <sys/time.h>

// fix SOCK_NONBLOCK for e.g. macOS

#ifndef SOCK_NONBLOCK
#include <fcntl.h>
#define SOCK_NONBLOCK O_NONBLOCK
#endif

typedef struct msg_event
{
  char *bfr;
  struct msg_event *next;
} msg_event_stc;

typedef struct
{
  int inport,outport,inskt,outskt;
  msg_event_stc *events;
  uint8_t leave_thread;
  pthread_mutex_t mtx;
  pthread_t thr;
} moogskt_stc;

static moogskt_stc *ms=NULL;
static void *thr(void *arg);
void lg(const char *fmt,...);

/*
 * The para to functions:
 * __declspec(dllexport)
 * seems not to be needed anymore, after adding two new linker paras in the winblows makefile
 */

#ifdef MOOG_W
static void bzero(void *bfr,int size)
{
  memset(bfr,0,size);
}
static int inet_aton(const char *cp,struct in_addr *addr)
{
  addr->s_addr=inet_addr(cp);
  return (addr->s_addr==INADDR_NONE) ? 0 : 1;
}
#endif

#define USEC_SLEEP 200000
#define MAX_WAIT 32
#define BFR_SIZE 1024
#define N_REM_ADDRESSES 4
#define PING_PORT 11339
#define REFBFR "0,199"

void moogskt_alive(char **address)
{
  int skt=socket(AF_INET,SOCK_DGRAM,0),res,i;
  struct sockaddr_in loc_addr,rem_addr;
  socklen_t addrlen;
  fd_set rset; 
  struct timeval tv={0,USEC_SLEEP};
  char bfr[BFR_SIZE],*rem_address;
  
  bzero(&loc_addr,sizeof(struct sockaddr_in));
  loc_addr.sin_family=AF_INET;
  loc_addr.sin_port=htons(PING_PORT);
  loc_addr.sin_addr.s_addr=INADDR_ANY;
  
  if(bind(skt,(struct sockaddr *)&loc_addr,sizeof(struct sockaddr_in))<0)
  {
    lg("%s: error binding loc_addr to ping port (%s)",__func__,strerror(errno));
    abort();
  }
  
  (*address)=NULL;

  FD_ZERO(&rset);
  FD_SET(skt,&rset);
  
  for(i=0;i<MAX_WAIT;i++)
  {
    res=select(skt+1,&rset,NULL,NULL,&tv);
    if(res>0)
    {
      addrlen=sizeof(struct sockaddr_in);

      res=recvfrom(skt,bfr,BFR_SIZE,0,(struct sockaddr *)&rem_addr,&addrlen);
      if(res<0)
      {
	lg("%s: error receiving from ping port (%s)",__func__,strerror(errno));
	abort();
      }
   
      if(memcmp(bfr,REFBFR,strlen(REFBFR)))
	continue; // strange: a bad packet from ping port
      
      rem_address=inet_ntoa(rem_addr.sin_addr);      

      (*address)=strdup(rem_address);
      break;
    }
  }
  
  close(skt);  
}

int moogskt_init(char *loc_address,char *rem_address1,char *rem_address2,char *rem_address3,char *rem_address4,int recv_port,int trans_port)
{
  if(ms!=NULL)
  {
    lg("Error! moogskt opened twice!");
    abort();
  }

  char *found_address=NULL,*rem_addresses[N_REM_ADDRESSES]={rem_address1,rem_address2,rem_address3,rem_address4};
  int progr_addr,i;

  moogskt_alive(&found_address);

  if(!found_address)
  {
    lg("%s: No platform commander is available.",__func__);    
    abort();
  }

  for(progr_addr=-1,i=0;i<N_REM_ADDRESSES;i++)
  {
    if(strlen(rem_addresses[i])<=0)
      continue;
    if(!strcmp(rem_addresses[i],found_address))
    {
      progr_addr=i;
      break;
    }
  }
  
  if(progr_addr<0)
  {
    lg("%s: No platform commander traffic found on required addresses (%s found).",__func__,found_address);
    abort();
  }

  free(found_address);

  struct sockaddr_in loc_addr,rem_addr;

  bzero(&loc_addr,sizeof(struct sockaddr_in));
  if(!inet_aton(loc_address,&loc_addr.sin_addr))
  {
    lg("%s: local address %s bad",__func__,loc_address);
    abort();
  }
    
  bzero(&rem_addr,sizeof(struct sockaddr_in));
  if(!inet_aton(rem_addresses[progr_addr],&rem_addr.sin_addr))
  {
    lg("%s: remote address %s bad",__func__,rem_addresses[progr_addr]);
    abort();
  }
  
  ms=malloc(sizeof(moogskt_stc));
  bzero(ms,sizeof(moogskt_stc));
  
  srand(time(NULL));
  
  ms->inport=rand()%10000+5000;
  ms->outport=ms->inport+1;

  ms->inskt=socket(AF_INET,SOCK_DGRAM,0);
  if(ms->inskt<0)
  {
    lg("%s: error opening in-socket (%s)",__func__,strerror(errno));
    abort();
  }
  
  loc_addr.sin_family=AF_INET;
  loc_addr.sin_port=htons(ms->inport);
  
  if(bind(ms->inskt,(struct sockaddr *)&loc_addr,sizeof(struct sockaddr_in))<0)
  {
    lg("%s: error binding loc_addr (%s) to %d (%d,%s)",__func__,loc_address,ms->inport,errno,strerror(errno));
    abort();
  }
  
  rem_addr.sin_family=AF_INET;
  rem_addr.sin_port=htons(recv_port);
  
  if(connect(ms->inskt,(struct sockaddr *)&rem_addr,sizeof(struct sockaddr_in))<0)
  {
    lg("%s: error connecting to %s:%d (%s)",__func__,rem_addresses[progr_addr],recv_port,strerror(errno));
    abort();
  }
  
//  ms->outskt=socket(AF_INET,SOCK_DGRAM|SOCK_NONBLOCK,SO_REUSEADDR);
  ms->outskt=socket(AF_INET,SOCK_DGRAM,0);
  if(ms->outskt<0)
  {
    lg("%s: error opening out-socket (%s)",__func__,strerror(errno));
    abort();
  }
  
  loc_addr.sin_port=htons(ms->outport);
  
  if(bind(ms->outskt,(struct sockaddr *)&loc_addr,sizeof(struct sockaddr_in))<0)
  {
    lg("%s: error binding out-socket to %d (%s)",__func__,ms->outport,strerror(errno));
    abort();
  }
  
  rem_addr.sin_port=htons(trans_port);
  
  if(connect(ms->outskt,(struct sockaddr *)&rem_addr,sizeof(struct sockaddr_in))<0)
  {
    lg("%s: error connecting out-socket to %s:%d (%s)",__func__,rem_addresses[progr_addr],trans_port,strerror(errno));
    abort();
  }

  int sts=pthread_mutex_init(&ms->mtx,NULL);
  if(sts)
  {
    lg("%s: error creating mutex (%s)",__func__,strerror(errno));
    abort();
  }
  sts=pthread_create(&ms->thr,NULL,thr,ms);
  if(sts)
  {
    lg("%s: error firing thread (%s)",__func__,strerror(errno));
    abort();
  }

  return 0;
}

int moogskt_close(void)
{
  if(ms==NULL)
  {
    lg("%s: Error! moogskt cannot be closed (not opened)!",__func__);
    abort();
  }

  ms->leave_thread=1;
  pthread_join(ms->thr,NULL);

  close(ms->inskt);
  close(ms->outskt);

  free(ms);

  ms=NULL;

  return 0;
}

int moogskt_send_pkt(int msg,char *payload)
{
  if(ms==NULL)
  {
    lg("%s: Error! moogskt: cannot send (not opened)!",__func__);
//    abort();
    return -1;
  }
  
  char bfr[(payload ? strlen(payload) : 0)+256];

  sprintf(bfr,"%d",msg);
  if(payload && strlen(payload)>0)
    sprintf(bfr+strlen(bfr),",%s",payload);

  if(send(ms->outskt,bfr,strlen(bfr),0)<0)
  {
    lg("%s: Error! moogskt: error sending [%s] (%s)",__func__,bfr,strerror(errno));
    abort();
  }

  return 0;
}

int moogskt_recvport(void)
{
  if(ms==NULL)
  {
    lg("%s: Error! moogskt has not been opened!",__func__);
    abort();
  }

  return ms->inport;
}

char *retv=NULL;

char *moogskt_recv_pkt(void)
{
  if(!ms || !ms->events)
    return NULL;

  msg_event_stc *evp;
  
  pthread_mutex_lock(&ms->mtx);
  evp=ms->events;
  ms->events=evp->next;
  pthread_mutex_unlock(&ms->mtx);

  retv=realloc(retv,strlen(evp->bfr)+1);
  strcpy(retv,evp->bfr);

  free(evp->bfr);
  free(evp);

  return retv;
}

int moogskt_recv_pkts(char ***pkts)
{
  (*pkts)=NULL;
  
  if(!ms || !ms->events)
    return 0;

  msg_event_stc *evp,*evp2;

  pthread_mutex_lock(&ms->mtx);
  evp=ms->events;
  ms->events=NULL;
  pthread_mutex_unlock(&ms->mtx);

  int n_events=0;

  while(evp)
  {
    evp2=evp;
    evp=evp2->next;
    
    (*pkts)=realloc(*pkts,sizeof(char *)*(n_events+1));
    
    (*pkts)[n_events]=malloc(strlen(evp2->bfr)+1);
    strcpy((*pkts)[n_events],evp2->bfr);
    n_events++;

    free(evp2->bfr);
    free(evp2);
  }

  return n_events;
}
  
static void *thr(void *arg)
{
  moogskt_stc *s=(moogskt_stc *)arg;
  fd_set rset; 
  int res;
  char *bfr=malloc(BFR_SIZE);
  msg_event_stc *ev,*evp;
  struct timeval tv={0,0};

  lg("Moogskt begin loop");

  FD_ZERO(&rset);
  while(!s->leave_thread)
  {
    FD_SET(s->inskt,&rset);
    tv.tv_usec=USEC_SLEEP;
    res=select(s->inskt+1,&rset,NULL,NULL,&tv);

    if(res>0)
    {
      res=recv(s->inskt,bfr,BFR_SIZE,0);
      ev=malloc(sizeof(msg_event_stc));
      
      ev->bfr=malloc(res+1);
      memcpy(ev->bfr,bfr,res);
      ev->bfr[res]=0;

      ev->next=NULL;

      pthread_mutex_lock(&s->mtx);
      
      if(!s->events)
	s->events=ev;
      else
      {
	for(evp=s->events;evp->next;evp=evp->next)
	  ;
	evp->next=ev;
      }

      pthread_mutex_unlock(&s->mtx);

      ev=NULL;
    }    
  }

  lg("Moogskt thread out.");
  free(bfr);
  
  return NULL;
}

void lg(const char *fmt,...)
{
  char tstr[17];
  time_t now=time(NULL);
  va_list ap;
  
  strftime(tstr,17,"[%y%m%d.%H%M%S] ",localtime(&now));
  fprintf(stderr,tstr);  
  
  va_start(ap,fmt);
  vfprintf(stderr,fmt,ap);
  va_end(ap);
  
  fputc('\n',stderr);
  fflush(stderr);
}

